CREATE PROCEDURE PA_ACTUALIZARCATEGORIA(
	@pnIdCategoria INT,
	@pcNombre VARCHAR(50), 
	@pcDescripcion VARCHAR(256)
)
AS 
BEGIN
	UPDATE CATEGORIA SET cNombre = @pcNombre, cDescripcion  = @pcDescripcion
    WHERE nIdCategoria = @pnIdCategoria
END
