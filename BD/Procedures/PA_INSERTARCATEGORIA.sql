CREATE PROCEDURE PA_INSERTARCATEGORIA(
	@pcNombre varchar(50), 
	@pcDescripcion varchar(256)
)
AS 
BEGIN
	INSERT INTO CATEGORIA(cNombre, cDescripcion)
	VALUES(@pcNombre, @pcDescripcion)
END
