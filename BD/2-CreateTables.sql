--TABLE CATEGORIA
CREATE TABLE CATEGORIA(
	nIdCategoria int identity(1,1), 
	cNombre varchar(50) not null,
	cDescripcion varchar(256) null 
)
GO
ALTER TABLE CATEGORIA
ADD CONSTRAINT PK_CATEGORIA
PRIMARY KEY(nIdCategoria)
GO

--TABLE PRESENTACION 
CREATE TABLE PRESENTACION(
	nIdPresentacion int identity(1,1),
	cNombre varchar(50) not null,
	cDescripcion varchar(256) 
)
GO

ALTER TABLE PRESENTACION
ADD CONSTRAINT PK_PRESENTACION
PRIMARY KEY(nIdPresentacion)
GO

--TABLE ARTICULO 
CREATE TABLE ARTICULO(
	nIdArticulo int identity(1,1), 
	cCodigo varchar(50) not null,
	cNombre varchar(50) not null,
	cDescripcion varchar(256) null,
	iImagen image null, 
	nIdCategoria int not null, 
	nIdPresentacion int not null,
)
GO

ALTER TABLE ARTICULO 
ADD CONSTRAINT PK_ARTICULO
PRIMARY KEY(nIdArticulo)
GO

ALTER TABLE ARTICULO 
ADD CONSTRAINT FK_ARTICULO_CATEGORIA
FOREIGN KEY(nIdCategoria) REFERENCES Categoria(nIdCategoria)
GO

ALTER TABLE ARTICULO 
ADD CONSTRAINT FK_ARTICULO_PRESENTACION
FOREIGN KEY(nIdPresentacion) REFERENCES PRESENTACION(nIdPresentacion)
GO

--TABLE PROVEEDOR
CREATE TABLE PROVEEDOR(
	nIdProveedor int identity(1,1),
	cRazonSocial varchar(150) not null, 
	cSectorComercial varchar(50) not null,
	cTipoDocumento varchar(20) not null, 
	cNumDocumento varchar(11) not null, 
	cDireccion varchar(100) null, 
	cTelefono varchar(10) null,
	cEmail varchar(50) null, 
	cUrl varchar(10) null
)
GO

ALTER TABLE PROVEEDOR 
ADD CONSTRAINT PK_PROVEEDOR
PRIMARY KEY(nIdProveedor)
GO

--TABLE TRABAJADOR
CREATE TABLE TRABAJADOR(
	nIdTrabajador int identity(1,1), 
	cNombre varchar(20) not null, 
	cApellidos varchar(40) not null,
	cSexo bit not null,
	dFechaNacimiento date not null,
	cNumDocumento varchar(8) not null, 
	cDireccion varchar(100) null, 
	cTelefono varchar(10) null,
	cEmail varchar(50) null, 
	cAcceso varchar(20) not null, 
	cUsuario varchar(20) not null, 
	cPassword varchar(20) not null 
)
GO 

ALTER TABLE TRABAJADOR 
ADD CONSTRAINT PK_TRABAJADOR 
PRIMARY KEY(nIdTrabajador)
GO

--TABLE INGRESO
CREATE TABLE INGRESO(
	nIdIngreso int identity(1,1),
	nIdTrabajador int not null,
	nIdProveedor int not null,
	dFecha date not null, 
	cTipoComprobante varchar(20) not null, 
	cSerie varchar(4) not null,
	cCorrelativo varchar(7) not null,
	nIgv decimal(4,2) not null,
)
GO

ALTER TABLE INGRESO 
ADD CONSTRAINT PK_INGRESO 
PRIMARY KEY(nIdIngreso)
GO

ALTER TABLE INGRESO 
ADD CONSTRAINT FK_INGRESO_TRABAJADOR 
FOREIGN KEY(nIdTrabajador) REFERENCES TRABAJADOR(nIdTrabajador)
GO

ALTER TABLE INGRESO 
ADD CONSTRAINT FK_INGRESO_PROVEEDOR 
FOREIGN KEY(nIdProveedor) REFERENCES PROVEEDOR(nIdProveedor)
GO

--TABLE DETALLEINGRESO
CREATE TABLE DETALLEINGRESO(
	nIdDetalleIngreso int identity(1,1), 
	nIdIngreso int not null,
	nIdArticulo int not null, 
	nPrecioCompra money not null,
	nPrecioVenta money not null, 
	nStockInicial int not null, 
	nStockActual int not null, 
	dFechaProduccion date not null,
	dFechaVencimiento date not null
)
GO

ALTER TABLE DETALLEINGRESO
ADD CONSTRAINT PK_DETALLEINGRESO
PRIMARY KEY(nIdDetalleIngreso)
GO

ALTER TABLE DETALLEINGRESO
ADD CONSTRAINT FK_DETALLEINGRESO_INGRESO
FOREIGN KEY(nIdIngreso) REFERENCES INGRESO(nIdIngreso)
ON DELETE CASCADE 
ON UPDATE CASCADE
GO

ALTER TABLE DETALLEINGRESO
ADD CONSTRAINT FK_DETALLEINGRESO_ARTICULO
FOREIGN KEY(nIdArticulo) REFERENCES ARTICULO(nIdArticulo)
GO

--TABLE CLIENTE 
CREATE TABLE CLIENTE(
	nIdCliente int identity(1,1), 
	cNombre varchar(50) not null, 
	cApellidos varchar(40) null, 
	cSexo bit null,
	dFechaNacimiento date not null, 
	cTipoDocumento varchar(20) not null, 
	cNumDocumento varchar(11) not null,
	cDireccion varchar(100) null,
	cTelefono varchar(10) null,
	cEmail varchar(50) null
)
GO

ALTER TABLE CLIENTE 
ADD CONSTRAINT PK_CLIENTE
PRIMARY KEY(nIdCliente)
GO

--TABLE VENTA
CREATE TABLE VENTA(
	nIdVenta INT IDENTITY(1,1),
	nIdCliente INT NOT NULL,
	nIdTrabajador INT NOT NULL,
	dFecha DATE NOT NULL,
	cTipoComprobante VARCHAR(20) NOT NULL,
	cSerie VARCHAR(4) NOT NULL,
	cCorrelativo VARCHAR(7) NOT NULL,
	nIgv DECIMAL(4,2) NOT NULL
)
GO

ALTER TABLE VENTA
ADD CONSTRAINT PK_VENTA
PRIMARY KEY(nIdVenta)
GO

ALTER TABLE VENTA
ADD CONSTRAINT FK_VENTA_CLIENTE
FOREIGN KEY(nIdCliente) REFERENCES CLIENTE(nIdCliente)
GO

ALTER TABLE VENTA
ADD CONSTRAINT FK_VENTA_TRABAJADOR
FOREIGN KEY(nIdTrabajador) REFERENCES TRABAJADOR(nIdTrabajador)
GO

--TABLE DETALLEVENTA
CREATE TABLE DETALLEVENTA(
	nIdDetalleVenta int identity(1,1),
	nIdVenta int not null, 
	nIdDetalleIngreso int not null, 
	nIdCantidad int not null,
	nPrecioVenta money not null,
	nDescuento money not null
)
GO
ALTER TABLE DETALLEVENTA 
ADD CONSTRAINT PK_DETALLEVENTA
PRIMARY KEY(nIdDetalleVenta)

ALTER TABLE DETALLEVENTA 
ADD CONSTRAINT FK_DETALLEVENTA_VENTA
FOREIGN KEY(nIdVenta) REFERENCES VENTA(nIdVenta)
GO

ALTER TABLE DETALLEVENTA 
ADD CONSTRAINT FK_DETALLEVENTA_DETALLEINGRESO
FOREIGN KEY(nIdDetalleIngreso) REFERENCES DETALLEINGRESO(nIdDetalleIngreso)
GO

 